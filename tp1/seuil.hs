-- seuilInt

-- seuilTuple

seuilInt :: Int -> Int -> Int -> Int
seuilInt min max x
        | x < min = min
        | x > max = max
        | otherwise = x

seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (min, max) x
        | x < min = min
        | x > max = max
        | otherwise = x

main :: IO ()
main = do
    putStrLn "TODO"
    print $ seuilInt 0 10 11
    print $ seuilTuple (0, 10) 11
