
-- fizzbuzz1 :: [Int] -> [String]

-- fizzbuzz2 :: [Int] -> [String]

-- fizzbuzz
fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 arr =
    fizzbuzz2_Internal arr []
    where
        fizzbuzz2_Internal :: [Int] -> [String] -> [String]
        fizzbuzz2_Internal [] acc = acc
        fizzbuzz2_Internal (head:tail) acc
            | ((mod head 3) == 0) && (mod head 5 == 0) = fizzbuzz2_Internal tail (acc ++ ["fizzbuzz"])
            | ((mod head 3) == 0)                      = fizzbuzz2_Internal tail (acc ++ ["fizz"])
            | ((mod head 5) == 0)                      = fizzbuzz2_Internal tail (acc ++ ["buzz"])
            | otherwise                                = fizzbuzz2_Internal tail (acc ++ [(show head)])


-- fizzbuzz

main :: IO ()
main = do
    putStrLn "TODO"
    print $ fizzbuzz2 [1..15]

