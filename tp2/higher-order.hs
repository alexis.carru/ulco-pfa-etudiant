import Data.List

-- threshList

-- selectList

-- maxList

threshList :: (Ord a) => a -> [a] -> [a]
threshList mx arr = map (min mx) arr

main = do
    print $ threshList 3 [1,2,3,4,5]
