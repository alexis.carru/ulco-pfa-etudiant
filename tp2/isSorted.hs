isSorted :: (Ord a) => [a] -> Bool
isSorted [] = True
isSorted [x] = True
isSorted (x:y:xs) = x <= y && isSorted (y:xs)

main :: IO ()
main = do
    let list = [1,2,5,4]
    print $ isSorted list