
-- mymap1 

-- mymap2

-- myfilter1 

-- myfilter2 

-- myfoldl 

-- myfoldr 

mymap1 :: (a -> a) -> [a] -> [a]
mymap1 _ [] = []
mymap1 fn (h:t) = (fn h) : (mymap1 fn t)  

-- mymap2
mymap2 :: (a -> a) -> [a] -> [a]
mymap2 fn list =
    mymap2_Internal fn list []
    where
        mymap2_Internal :: (a -> a) -> [a] -> [a] -> [a]
        mymap2_Internal _ [] acc = acc
        mymap2_Internal fn (h:t) acc = mymap2_Internal fn t (acc ++ [(fn h)]) 

myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = []
myfilter1 func (h:t)
    | func h = (h:myfilter1 func t)
    | otherwise = myfilter1 func t

myfilter2 :: (a -> Bool) -> [a] -> [a]
myfilter2 fn list =
    myfilter2_Internal list []
    where
        myfilter2_Internal [] acc = acc
        myfilter2_Internal (h:t) acc
            | (fn h)      = myfilter2_Internal t (acc ++ [h])
            | otherwise   = myfilter2_Internal t acc

main :: IO ()
main = putStrLn "TODO"

