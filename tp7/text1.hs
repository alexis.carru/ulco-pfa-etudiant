import Data.ByteString.Char8 as BS hiding (putStrLn)
import Prelude 

main :: IO ()
main =  do
    file <- BS.readFile "test1.hs"
    let str = unpack file
    putStrLn str