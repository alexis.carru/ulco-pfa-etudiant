type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
mySqrt x = if x < 0 then Left "mySqrt < 0" else Right (sqrt x)

myLog :: Double -> MyNum
myLog x = if x <= 0 then Left "mySqrt <= 0" else Right (log x)

myMul2 :: Double -> MyNum
myMul2 = return . (*2)

myNeg :: Double -> MyNum
myNeg x = Right (-x)

myCompute :: MyNum
myCompute =
    case mySqrt 16 of
        Left err -> Left err
        Right x -> case myNeg x of
            Left e -> Left e
            Right y -> myMul2 y

main :: IO ()
main = putStrLn $ showMyNum myCompute