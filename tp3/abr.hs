import Data.List (foldl')

data Abr = Feuille | Noeud Int Abr Abr

insererAbr :: Int -> Abr -> Abr
insertAbr x Feuille = Noeud x Feuille Feuille
insererAbr x (Noeud y ag ad) =
    if x < y
    then Noeud y (insererAbr x ag) ad
    else Noeud y (insererAbr x ad) ad

listToAbr :: [Int] -> Abr
listToAbr = foldr insererAbr Feuille

abrToList :: Abr -> [Int]
abrToList Feuille = []
abrToList (Noeud x ag ad) = abrToList ag ++ [x] ++ abrToList ad


main :: IO ()
main = do
    print $ insererAbr 13 (insererAbr 3 (insererAbr 12 Feuille))
    print $ listToAbr [13, 3 ,12]
