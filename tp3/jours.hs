
-- TODO Jour

-- estWeekend :: Jour -> Bool

-- compterOuvrables :: [Jour] -> Int

data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche


estWeekend :: Jour -> Bool 
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables jours = compterOuvrables_Internal jours 0
    where
        compterOuvrables_Internal [] acc = acc
        compterOuvrables_Internal (h:t) acc
            | estWeekend(h)   = compterOuvrables_Internal t acc
            | otherwise       = compterOuvrables_Internal t (acc + 1)

main :: IO ()
main = putStrLn "TODO"


