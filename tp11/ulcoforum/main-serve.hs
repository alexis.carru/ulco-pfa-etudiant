{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Control.Monad
import Control.Monad.IO.Class
import Database.Selda.SQLite 
import Web.Scotty
import Lucid

import Thread

dbFilename :: String
dbFilename = "discussion.db"

main :: IO ()
main = scotty 3000 $ do

    get "/" $ do
        html $ renderText $ ulcoForum

    get "/messages" $ do
        messages <- liftIO $ withSQLite dbFilename dbSelectAllMessage
        html $ renderText $ listMessages messages

    get "/threads" $ do
        threads <- liftIO $ withSQLite dbFilename dbSelectAllThread
        html $ renderText $ listThreads threads


    
listMessages :: [Message] -> Html ()
listMessages messages = do
    homeHtml
    h1_ "Messages"
    ul_ $ forM_ messages $ \m -> li_ $ do
        toHtml $ message_text m

listThreads :: [Thread] -> Html ()
listThreads threads = do
    homeHtml
    h1_ "Threads"
    ul_ $ forM_ threads $ \t -> li_ $ do
        toHtml $ thread_name t

     

ulcoForum :: Html()
ulcoForum = do
    homeHtml
    p_ "This is ulcoforum"

homeHtml :: Html()
homeHtml = do 
    doctype_
    html_ $ do
        head_ (meta_ [charset_ "utf-8"] )
        body_ $ do
            h1_ (a_ [href_ "/"] ("UlcoForum")) 
            p_ $ do
                a_ [href_ "messages"] ("alldata")
                " - "
                a_ [href_ "/threads"] ("allthreads")
            hr_ []
