{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Thread where

import Control.Monad.IO.Class (liftIO)
import Database.Selda
import Database.Selda.SQLite

----------------------------------------------------------------------
-- Message
----------------------------------------------------------------------

data Message = Message
  { message_id :: ID Message
  , message_text :: Text
  , message_id_Thread :: ID Thread
  } deriving (Generic, Show)

instance SqlRow Message

message_table :: Table Message
message_table = table "message" [#message_id :- autoPrimary, #message_id_Thread :- foreignKey thread_table #thread_id]

----------------------------------------------------------------------
-- Thread
----------------------------------------------------------------------

data Thread = Thread
  { thread_id :: ID Thread
  , thread_name :: Text
  } deriving (Generic, Show)

instance SqlRow Thread

thread_table :: Table Thread
thread_table = table "thread" [#thread_id :- autoPrimary]

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------

dbInit :: SeldaT SQLite IO ()
dbInit = do

    createTable thread_table
    tryInsert thread_table 
      [Thread def "Convo 1"] >>= liftIO . print
  
    createTable message_table
    tryInsert message_table
        [ Message def "coucou" (toId 1)
        , Message def "salut"  (toId 1)
        , Message def "au revoir" (toId 1)
        , Message def "bye bye" (toId 1) ]
        >>= liftIO . print

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------
dbSelectAllMessage :: SeldaT SQLite IO [Message]
dbSelectAllMessage = query $ select message_table


dbSelectAllThread :: SeldaT SQLite IO [Thread]
dbSelectAllThread = query $ select thread_table



selectAllData :: SeldaT SQLite IO [Message :*: Thread]
selectAllData = query $ do
    messages <- select message_table
    threads <- select thread_table
    restrict (messages ! #message_id_Thread .== threads ! #thread_id)
    return (messages :*: threads)