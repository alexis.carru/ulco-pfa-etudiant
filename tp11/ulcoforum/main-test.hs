{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (when)
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (sqliteOpen, seldaClose)
import System.Directory (doesFileExist)

import Thread

dbFilename :: String
dbFilename = "discussion.db"

main :: IO ()
main = do

    dbExists <- doesFileExist dbFilename
    conn <- sqliteOpen dbFilename
    when (not dbExists) $ runSeldaT dbInit conn

    
    runSeldaT Thread.selectAllData conn >>= mapM_ print
    
    seldaClose conn